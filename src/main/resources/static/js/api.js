angular
    .module('third_party')
    .service("api", function () {

        var baseUrl = window.location.href.substr(0,
            window.location.href.length - window.location.hash.length - 1);

        this.url = {
            base: baseUrl,
            apxAPI: baseUrl + '/api/APX',
            regelAPI: baseUrl + '/api/Regel'
        };
    });