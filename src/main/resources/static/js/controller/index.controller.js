angular
    .module('third_party')
    .controller('indexController', ['$scope', '$http', '$filter', 'api', function ($scope, $http, $filter, api) {

        $scope.date = "";
        $scope.dateRegel = "";
        $scope.regelData = [];
        $scope.apxData = [];

        function getAPXData() {

            $http({
                method: 'GET',
                url: api.url.apxAPI
            }).then(function (response) {
                $scope.date = $filter('date')(response.data.date, 'dd-MM-yyyy HH:mm');
                $scope.apxData = response.data.value;
            })
        }

        function getRegelData() {

            $http({
                method: 'GET',
                url: api.url.regelAPI
            }).then(function (response) {
                $scope.dateRegel = response.data.date;

                if(typeof response.data.recordList !== 'undefined' && response.data.recordList.length > 0){
                    $scope.regelData = response.data.recordList;
                } else {
                    $scope.dateRegel = response.data.date + ' Data is not yet available';
                }
            })
        }

        function init() {
            getAPXData();
            getRegelData();
        }

        init();

    }]);