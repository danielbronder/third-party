module.exports = function (grunt) {
    grunt.initConfig({
        concat: {
            javascript: {
                src: [
                    "bower_components/angular/angular.min.js",
                    "bower_components/angular-aria/angular-aria.min.js",
                    "bower_components/angular-animate/angular-animate.min.js",
                    "bower_components/angular-messages/angular-messages.min.js",
                    "bower_components/angular-material/angular-material.min.js",
                    "bower_components/angular-material-data-table/dist/md-data-table.min.js"
                ],
                dest: "js/gruntscripts.js"
            },
            styling: {
                src: [
                    "bower_components/angular-material/angular-material.min.css",
                    "bower_components/angular-material-data-table/dist/md-data-table.min.css"

                ],
                dest: "css/gruntstyles.css"
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.registerTask('default', ['concat']);
};