package nl.etpa.third_party.model;

import java.util.List;

public class Imbalance {

    private String date;
    private List<Record> recordList;

    public Imbalance() {
    }

    public Imbalance(String date, List<Record> recordList) {
        this.date = date;
        this.recordList = recordList;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<Record> getRecordList() {
        return recordList;
    }

    public void setRecordList(List<Record> recordList) {
        this.recordList = recordList;
    }
}