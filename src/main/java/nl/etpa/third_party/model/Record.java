package nl.etpa.third_party.model;

public class Record {

    private String PTU;
    private String from;
    private String to;
    private String upwardsDispatch;
    private String downwardsDispatch;
    private String takeFromSystem;
    private String feedIntoSystem;
    private String regulationState;

    public Record() {

    }

    public Record(String PTU, String from, String to, String upwardsDispatch, String downwardsDispatch, String takeFromSystem, String feedIntoSystem, String regulationState) {
        this.PTU = PTU;
        this.from = from;
        this.to = to;
        this.upwardsDispatch = upwardsDispatch;
        this.downwardsDispatch = downwardsDispatch;
        this.takeFromSystem = takeFromSystem;
        this.feedIntoSystem = feedIntoSystem;
        this.regulationState = regulationState;
    }

    public String getPTU() {
        return PTU;
    }

    public void setPTU(String PTU) {
        this.PTU = PTU;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getUpwardsDispatch() {
        return upwardsDispatch;
    }

    public void setUpwardsDispatch(String upwardsDispatch) {
        this.upwardsDispatch = upwardsDispatch;
    }

    public String getDownwardsDispatch() {
        return downwardsDispatch;
    }

    public void setDownwardsDispatch(String downwardsDispatch) {
        this.downwardsDispatch = downwardsDispatch;
    }

    public String getTakeFromSystem() {
        return takeFromSystem;
    }

    public void setTakeFromSystem(String takeFromSystem) {
        this.takeFromSystem = takeFromSystem;
    }

    public String getFeedIntoSystem() {
        return feedIntoSystem;
    }

    public void setFeedIntoSystem(String feedIntoSystem) {
        this.feedIntoSystem = feedIntoSystem;
    }

    public String getRegulationState() {
        return regulationState;
    }

    public void setRegulationState(String regulationState) {
        this.regulationState = regulationState;
    }
}