package nl.etpa.third_party.model;

import java.util.List;

public class APX {

    private long date;
    private List<Value> value;

    public APX() {
    }

    public APX(long date, List<Value> value) {
        this.date = date;
        this.value = value;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public List<Value> getValue() {
        return value;
    }

    public void setValue(List<Value> value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "data {" +
                " date : " + date +
                " value : " + value + "\n}";
    }
}