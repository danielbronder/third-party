package nl.etpa.third_party.model;

public class Value {

    private String order;
    private String hour;
    private String price;
    private String volume;

    public Value() {
    }

    public Value(String number, String hour, String price, String volume) {
        this.order = number;
        this.hour = hour;
        this.price = price;
        this.volume = volume;
    }

    public String getNumber() {
        return order;
    }

    public void setNumber(String number) {
        this.order = number;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }


    @Override
    public String toString() {
        return "Value : { " +
                "order: " + order +
                " hour: " + hour +
                " price: " + price  +
                " volume: " + volume +
                " }";
    }
}