package nl.etpa.third_party.controller;

import nl.etpa.third_party.model.Imbalance;
import nl.etpa.third_party.model.Record;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@RestController
public class RegelController {

    @GetMapping(value = "/api/Regel", produces = "application/json")
    public Imbalance getAll() {

        List<Record> records = new ArrayList<>();
        Record record;
        LocalDate localDate = LocalDate.now();

        DayOfWeek dayOfWeek = localDate.getDayOfWeek();

        switch (dayOfWeek) {

            case SUNDAY:

                localDate = localDate.minusDays(2);

            default:

                localDate = localDate.minusDays(1);
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        String date = localDate.format(formatter);
        

        String url = "http://www.tennet.org/bedrijfsvoering/ExporteerData.aspx?exporttype=verrekenprijzen&format=xml&datefrom=" + date + "&dateto=" + date + "&submit=1";

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            Document document = builder.parse(url);

            document.getDocumentElement().normalize();

            Element root = document.getDocumentElement();

            NodeList nList = root.getChildNodes();
            Node Record = nList.item(1);

            NodeList nodeList = Record.getChildNodes();

            for (int x = 0; x < nodeList.getLength(); x++) {

                Node node = nodeList.item(x);

                if (node.getNodeType() == Node.ELEMENT_NODE) {

                    Element element = (Element) node;

                    record = new Record();

                    record.setPTU(element.getElementsByTagName("PTU").item(0).getTextContent());
                    record.setFrom(element.getElementsByTagName("PERIOD_FROM").item(0).getTextContent());
                    record.setTo(element.getElementsByTagName("PERIOD_UNTIL").item(0).getTextContent());

                    if (element.getElementsByTagName("UPWARD_DISPATCH").item(0) == null) {
                        record.setUpwardsDispatch(null);
                    } else {
                        record.setUpwardsDispatch(element.getElementsByTagName("UPWARD_DISPATCH").item(0).getTextContent());
                    }

                    if (element.getElementsByTagName("DOWNWARD_DISPATCH").item(0) == null) {
                        record.setUpwardsDispatch(null);
                    } else {
                        record.setDownwardsDispatch(element.getElementsByTagName("DOWNWARD_DISPATCH").item(0).getTextContent());
                    }

                    record.setTakeFromSystem(element.getElementsByTagName("TAKE_FROM_SYSTEM").item(0).getTextContent());
                    record.setFeedIntoSystem(element.getElementsByTagName("FEED_INTO_SYSTEM").item(0).getTextContent());
                    record.setRegulationState(element.getElementsByTagName("REGULATION_STATE").item(0).getTextContent());

                    if (record.getRegulationState().equals("2")){
                        records.add(record);
                    }

                }
            }

        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }

        return new Imbalance(date, records);
    }
}