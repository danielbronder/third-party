package nl.etpa.third_party.controller;

import nl.etpa.third_party.model.APX;
import nl.etpa.third_party.model.Value;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@RestController
public class APXController {

    @GetMapping(value = "/api/APX", produces = "application/json")
    public APX getAll() {

        APX apx = new APX();
        List<Value> valueses = new ArrayList<>();

        List<String> list;
        Value v;

        try {
            URL url = new URL("https://www.apxgroup.com/rest-api/quotes/APX%20Power%20NL%20Hourly?type=latest&limit=1");
            JSONTokener tokener = new JSONTokener(url.openStream());
            JSONObject jsonObject = new JSONObject(tokener);

            JSONArray quote = (JSONArray) jsonObject.get("quote");

            for (int i = 0; i < quote.length(); i++) {

                JSONObject quotes = (JSONObject) quote.get(i);
                apx.setDate((Long) quotes.get("date_applied"));
                JSONArray values = (JSONArray) quotes.get("values");

                list = new ArrayList<>();

                for (int y = 0; y < values.length(); y++) {

                    JSONObject value = (JSONObject) values.get(y);
                    list.add((String) value.get("value"));

                }

                v = new Value();
                v.setNumber(list.get(0));
                v.setHour(list.get(1));
                v.setVolume(list.get(2));
                v.setPrice(list.get(3));

                valueses.add(v);
            }

            apx.setValue(valueses);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return apx;
    }
}